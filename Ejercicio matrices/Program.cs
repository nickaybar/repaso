﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_matrices
{
	class Program
	{

		public static void CargarMatriz(int[,] mat) // carga la matriz, funcion sin retorno

		{
			Random generador = new Random();

			for (int _f = 0; _f < mat.GetLength(0); _f++) //mat getlenght (0) hace referencia al numero de filas
			{
				for (int _c = 0; _c < mat.GetLength(1); _c++) // (1) hace referencia al numero de columnas
				{
					mat[_f, _c] = generador.Next(10);
				} //for c
			}//for f
		}


		
		public static void CargarMatrizAsistencia(int[,] mat) // carga la matriz, funcion sin retorno

		{
			Random generador = new Random();
			int chancePresente = 0;


			for (int _f = 0; _f < mat.GetLength(0); _f++) //mat getlenght (0) hace referencia al numero de filas
			{
				for (int _c = 0; _c < mat.GetLength(1); _c++) // (1) hace referencia al numero de columnas
				{
					chancePresente = generador.Next(100);
					if(chancePresente <= 79)
					{
						mat[_f, _c] = 1;
					}
					else
					{
						mat[_f, _c] = 0;
					}
				} //for c
			}//for f
		}



		public static void MostrarParesMatriz(int[,] mat) // muestra la matriz

		{
			for (int _f = 0; _f < mat.GetLength(0); _f++) //mat getlenght (0) hace referencia al numero de filas
			{
				for (int _c = 0; _c < mat.GetLength(1); _c++) // (1) hace referencia al numero de columnas
				{
					if (mat[_f, _c] % 2 == 0)
					{
						Console.ForegroundColor = ConsoleColor.Yellow;
					}
					else
					{
						Console.ResetColor();
					}
					Console.Write("{0} ", mat[_f, _c].ToString("00"));
				} //for c
				Console.WriteLine();
				System.Threading.Thread.Sleep(50); // comando para agregarle retraso ahre
			}//for f
		}


		public static void MostrarMatriz(int[,] mat) // muestra la matriz

		{
			for (int _f = 0; _f < mat.GetLength(0); _f++) //mat getlenght (0) hace referencia al numero de filas
			{
				for (int _c = 0; _c < mat.GetLength(1); _c++) // (1) hace referencia al numero de columnas
				{
					Console.Write("{0} ", mat[_f, _c].ToString("00"));
					System.Threading.Thread.Sleep(50); // comando para agregarle retraso ahre
				} //for c
				Console.WriteLine();
				System.Threading.Thread.Sleep(50); // comando para agregarle retraso ahre
			}//for f
		}


		public static void MostrarMatrizAsistencia(int[,] mat) // muestra la matriz de asistencias ya cargada

		{
			for (int _f = 0; _f < mat.GetLength(0); _f++) //mat getlenght (0) hace referencia al numero de filas
			{
				for (int _c = 0; _c < mat.GetLength(1); _c++) // (1) hace referencia al numero de columnas
				{
					if(mat[_f,_c] == 1)
					{
						Console.ForegroundColor = ConsoleColor.Green;
						Console.Write("P");
					}
					else
					{
						Console.ForegroundColor = ConsoleColor.Red;
						Console.Write("A");
					}
				} 
			}
		}


		public static int MejorVendedor(int[,] mat) // calculas y sumar las 3 filas de forma independiente
		{
			int _res = 0;

			int maximo = int.MinValue;

			int suma = 0;


			for (int f= 0; f < mat.GetLength(0); f++)
			{
				suma = SumarFilasMatriz(mat, f);

				if(suma > maximo)
				{
					_res = f;
					maximo = suma;
				}

			} // for filas


			return _res;
		}



		public static int SumarMatriz(int[,] mat) // funcion int para sumar matrices

		{
			int _resultado = 0;

			_resultado = 0; // establece el resultado en 0, como rol de acumulador antes del for
			for (int _f = 0; _f < mat.GetLength(0); _f++) //mat getlenght (0) hace referencia al numero de filas
			{
				for (int _c = 0; _c < mat.GetLength(1); _c++) // (1) hace referencia al numero de columnas
				{
					_resultado += mat[_f, _c];
				} //for c

			}//for f
			return _resultado;
		}

		public static int SumarColumnaMatriz(int[,] mat, int col) // sumar columnas

		{
			int _resultado = 0;

			_resultado = 0; // establece el resultado en 0, como rol de acumulador antes del for
			for (int _f = 0; _f < mat.GetLength(0); _f++) //mat getlenght (0) hace referencia al numero de filas
			{

				_resultado += mat[_f, col];

			}//for f
			return _resultado;
		}

		public static int SumarFilasMatriz(int[,] mat, int fil) // sumar filas

		{
			int _resultado = 0;

			_resultado = 0; // establece el resultado en 0, como rol de acumulador antes del for
			for (int _c = 0; _c < mat.GetLength(1); _c++) //mat getlenght (1) hace referencia al numero de columnas
			{

				_resultado += mat[fil, _c];

			}//for f
			return _resultado;
		}


		public static double PromedioMatriz(int[,] mat) // funcion int para promediar la suma de matrices

		{
			double _resultado = 0.0;
			int _sumatoria = 0;

			_sumatoria = 0; // establece el resultado en 0, como rol de acumulador antes del for
			for (int _f = 0; _f < mat.GetLength(0); _f++) //mat getlenght (0) hace referencia al numero de filas
			{
				for (int _c = 0; _c < mat.GetLength(1); _c++) // (1) hace referencia al numero de columnas
				{
					_sumatoria += mat[_f, _c];
				} //for c

			}//for f
			_resultado = (double)_sumatoria / (mat.Length);
			return _resultado;
		}


		public static void EncimaPromedioMatriz(int[,] mat, double promedio) // funcion int para promediar la suma de matrices

		{
			int destacados = 0;


			destacados = 0;
			for (int _f = 0; _f < mat.GetLength(0); _f++) //mat getlenght (0) hace referencia al numero de filas
			{
				for (int _c = 0; _c < mat.GetLength(1); _c++) // (1) hace referencia al numero de columnas
				{
					if (mat[_f, _c] >= promedio)
					{
						Console.ForegroundColor = ConsoleColor.Green;
						destacados++;
					}
					else
					{
						Console.ResetColor();
					}
					Console.Write(mat[_f, _c].ToString("00") + " ");
				} //for c

			}//for f
		}


		static void Main(string[] args)
		{

			const int COLS = 16; // clases
			const int FILS = 10; // alumnos

			int[,] _matriz = new int[FILS, COLS];

			int _suma = 0;
			double _promedio = 0;
			int _indice = 0;

			Console.Clear();
			Console.ResetColor();


		//	CargarMatriz(_matriz); // se pasa la matriz a la funcion CargarMatriz
		//	MostrarMatriz(_matriz); // mostrar la matriz
			//_indice = MejorVendedor(_matriz);

			CargarMatrizAsistencia(_matriz);
			MostrarMatrizAsistencia(_matriz);
		//	Console.WriteLine($"El mejor vendedor fue {_indice}");


			/*
			MostrarParesMatriz(_matriz); // mostrar matriz pares
			
			Console.WriteLine();
			
			_suma = SumarMatriz(_matriz);
			Console.WriteLine("Suma vale: {0}", _suma);

			_promedio = PromedioMatriz(_matriz);
			Console.WriteLine("Promedio vale: {0}", _promedio.ToString("0.00"));

			_suma = SumarColumnaMatriz(_matriz, 0);
			Console.WriteLine("Suma columna vale: {0}", _suma);

	        */


			Console.ReadKey();
		}
	}
}