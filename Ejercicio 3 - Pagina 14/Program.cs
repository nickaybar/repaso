﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_2___Pagina_14
{
    class Program
    {
        static void Main(string[] args)
        {
            // Declarar
            decimal _numero = 0m;
            decimal _maximo = decimal.MinValue;
            bool _bandera = false;
            int _i = 0;


            //operar

            Console.WriteLine("Ingrese los numeros que usted desee para determinar el mayor ( Presione -1 para finalizar ) :");
            while (_bandera == false)
            {
                Console.Write($"Ingrese el numero {_i + 1}: ");

                if (decimal.TryParse(Console.ReadLine(), out _numero) && _numero >= -1)
                {
                    {
                        if (_numero == -1) // Condicion para terminar
                        {
                            Console.WriteLine("El programa ha finalizado.");
                            _bandera = true;
                        }
                        else
                        {

                            if (_numero > _maximo) // condicion mayor

                            {
                                _maximo = _numero;

                            }
                            _i++; // i++ fuera del if, porque sino nunca va a sumar
                        }
                    }
                }
                else
                {
                    Console.WriteLine("El numero ingresado es negativo.");
                }
            }
            Console.WriteLine();
            Console.WriteLine($"El mayor numero ingresado es {_maximo}");
            Console.ReadKey();
        }
    }
}
