﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_vectores_4
{
    class Program
    {
        public static void CargarVector(int[] v, int N) // PARAMETROS SIN _ PARA DISTINGUIR DE LAS VARIABLES
        {
            Random _generador = new Random(); // generador aleatorio
            for (int _i = 0; _i < N; _i++)
            {
                v[_i] = _generador.Next(100);
            }
        }
        static void Main(string[] args)
        {
            const int ELEMENTOS = 100;
            int[] _vec = new int[ELEMENTOS];

            int _n = 0;

            Console.Clear();
            Console.WriteLine("Celdas a utilizar: ");
            _n = Convert.ToInt32(Console.ReadLine());



            CargarVector(_vec, _N);


            Console.ReadKey();
        }
    }
}
