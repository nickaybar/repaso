﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panaderia
{
	class Program
	{

		public static void CargaMatriz(int[] vec)
		{
			Random generador = new Random();
			for (int i = 0; i < vec.Length; i++)
			{
				vec[i] = generador.Next(5);
			}
		}

		public static void MostrarMatriz(int[] vec)
		{
			Random generador = new Random();
			for (int i = 0; i < vec.Length; i++)
			{
				Console.Write($"{vec[i]} ");
			}
		}
		public static void SumaMatriz(int[] vec)
		{
			int _primera = 0;
			int _segunda = 0;
			
			for (int i = 0; i <= 14; i++) // primera quincena
			{
				_primera += vec[i];

			}
			for (int i = 15; i < 30; i++) // segunda quincena
			{
				_segunda += vec[i];

			}
			Console.WriteLine();
			Console.WriteLine();
			if (_primera > _segunda)
			{
				Console.WriteLine($"Se vendio mas en la primera quincena con un total de {_primera} kilos");
			}
			else
			{
				Console.WriteLine($"Se vendio mas en la segunda quincena con un total de {_segunda} kilos");
			}
		}

		public static int Ganancia(int[] vec, int precio)
		{
			int _res = 0;


			for (int i = 0; i < vec.Length; i++)
			{
				_res += vec[i];
			}

			_res = _res * precio;

			return _res;
		}

		static void Main(string[] args)
		{
			const int DIAS = 30;
			const int PRECIO = 30;
			int[] vec = new int[DIAS];

			int _ganancia = 0;

			CargaMatriz(vec);
			MostrarMatriz(vec);
			SumaMatriz(vec);


			_ganancia = Ganancia(vec, PRECIO);

			Console.WriteLine($"\nLa ganancia total es de ${_ganancia}");

			Console.ReadKey();
		}
	}
}
