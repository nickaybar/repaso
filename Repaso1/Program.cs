﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repaso1
{
    class Program
    {
        static void Main(string[] args)
        {
            // numero primo = se divide por si mismo y por 1
            int _numero = 0; // numero a analizar si es primo o no
            int _divisor = 0; // actua como el divisor del numero ingresado
            int _divisores = 0; // almacena la cantidad de divisores

            int _minimo = 0;
            int _maximo = 0;
            Console.Clear();

            // operamos


            Console.WriteLine("Minimo: ");
            _minimo = Convert.ToInt32(Console.ReadLine()); // convierto el numero en entero

            Console.WriteLine("Maximo: ");
            _maximo = Convert.ToInt32(Console.ReadLine()); // convierto el numero en entero


            for (int _i = _minimo; _i <= _maximo; _i++) // ciclo for para ingresar un valor minimo y maximo
            {
                _numero = _i;
                Console.WriteLine();
                Console.WriteLine($"El numero a analizar es {_numero}");


                //----------------------------------------//
                Console.WriteLine();

                _divisores = 0; // declaro el divisores para que comience en 0
                for (_divisor = 1; _numero >= _divisor; _divisor++) // ciclo repetitivo para descubrir si el numero ingresado es primo o no
                {
                    if (_numero % _divisor == 0) // si el resto del numero/ divisor = 0, aumenta la cantidad de divisores(contador)
                    {

                        Console.WriteLine($"El {_numero} se puede dividir en {_divisores}"); // muestra cuantos divisores tiene el numero ingresado
                        _divisores++; // debido a que supero el if, aumenta la cantidad de divisores
                    }
                } // for
                Console.WriteLine();
                

                //----------------------------------------//

                if (_divisores == 2)
                {
                    Console.WriteLine($"El numero {_numero} es primo");
                }
                else
                {

                    Console.WriteLine($"El numero {_numero} no es primo");

                }
            }
            //----------------------------------------//
            Console.ReadKey();
        }
    }
}
