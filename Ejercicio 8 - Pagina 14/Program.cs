﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_8___Pagina_14
{
	class Program
	{
		static void Main(string[] args)
		{

			//Declarar variables

			decimal _numero = 0;
			decimal _suma = 0;
			decimal _promedio = 0;
			int _contador = 0;
			bool _finalizarPrograma = false;


			Console.WriteLine("Suma y promedio de numeros. Presione 0 cuando desee terminar");
			while (_finalizarPrograma == false)
			{
				Console.Write("Ingrese un numero: ");
				if (decimal.TryParse(Console.ReadLine(), out _numero))
				{
					if (_numero == 0)
					{
						_finalizarPrograma = true;
					}
					else
					{
						if (_numero % 2 == 0 && _numero < 0)
						{
							_contador++;
							_suma += _numero;
							_promedio = _suma / _contador;
						}

					}
				}
			}
			Console.WriteLine($"La suma de todos los numeros ingresados es {_suma}");
			Console.WriteLine($"El promedio de los numeros ingresados es {_promedio}");
			Console.ReadKey();
		}
	}
}
