﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemploNuevo
{
    class Program
    {
        public static int Producto(ref int _a, int _b) // parametros formales
        {
            int _resultado = 0;
            _resultado = _a * _b; // parametros actuales
            _a = 10;
            _b = 20;

            return _resultado;
        }
        static void Main(string[] args)
        {
            int _n1 = 3;
            int _n2 = 5;
            int _res = 0;

            Console.Clear();

            _res = Producto(ref _n1, _n2);

            Console.WriteLine($"_n1 vale {_n1} y _n2 vale {_n2}. El producto de ellos es {_res}");






            Console.ReadKey();
        }
    }
}
