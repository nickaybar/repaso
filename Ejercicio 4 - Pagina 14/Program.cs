﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_2___Pagina_14
{
    class Program
    {
        static void Main(string[] args)
        {
            // Declarar
            decimal _suma = 0;
            decimal _promedio = 0;
            int _contador = 0;
            decimal _numero = 0m;
            bool _bandera = false;
            int _i = 0;


            //operar

            Console.WriteLine("Ingrese los numeros que usted desee promediar ( Presione 0 para finalizar ) :");
            while (_bandera == false)
            {
                Console.Write($"Ingrese el numero {_i + 1}: ");

                if (decimal.TryParse(Console.ReadLine(), out _numero) && _numero >= 0)
                {
                    {
                        if (_numero == 0) // Condicion para terminar
                        {
                            Console.WriteLine("El programa ha finalizado.");
                            _bandera = true;
                        }
                        else
                        {
                            _suma += _numero;

                            _i++;
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Ocurrio un error. Por favor ingrese un numero positivo");
                }
            }

            Console.WriteLine();
            Console.WriteLine($"La suma de los numeros ingresados es {_suma}");
            Console.ReadKey();
        }
    }
}
