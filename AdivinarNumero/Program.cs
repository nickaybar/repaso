﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdivinarNumero
{
    class Program
    {
        static void Main(string[] args)
        {
            // declaracion variables
            int _target = 0; // numero generado por la machine
            int _numero = 0; // numero a ingresar
            int _intentos = 0; // contador de intentos
            int _maximointentos = 0; // maximo intentos
            Random _generador = new Random();

            Console.Clear();

            // OPERAR
            _maximointentos = 3;
            _target = _generador.Next(10);
            Console.WriteLine("Adivina el numero.");

            _intentos = 0;
            do
            {
                Console.WriteLine("Numero : ");
                _numero = Convert.ToInt32(Console.ReadLine());
                _intentos++;

            } while ((_numero != _target) && ( _intentos < _maximointentos));
            if(_intentos <= _maximointentos)
            {
                Console.WriteLine($"Usted adivino el numero en {_intentos} intentos");
            }
            else
            {
                Console.WriteLine($"El numero era {_target}");
                Console.WriteLine("Usted a superado el numero de intentos.");
            }



            Console.ReadKey();
        }
    }
}

