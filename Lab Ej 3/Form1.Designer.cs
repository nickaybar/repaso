﻿namespace Lab_Ej_3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnMostrar = new System.Windows.Forms.Button();
            this.txtDisplay = new System.Windows.Forms.TextBox();
            this.txtComparacion = new System.Windows.Forms.TextBox();
            this.lblContador = new System.Windows.Forms.Label();
            this.lblContadas = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnMostrar
            // 
            this.btnMostrar.Location = new System.Drawing.Point(275, 235);
            this.btnMostrar.Name = "btnMostrar";
            this.btnMostrar.Size = new System.Drawing.Size(157, 97);
            this.btnMostrar.TabIndex = 0;
            this.btnMostrar.Text = "Mostrar Valor vector";
            this.btnMostrar.UseVisualStyleBackColor = true;
            this.btnMostrar.Click += new System.EventHandler(this.btnMostrar_Click);
            // 
            // txtDisplay
            // 
            this.txtDisplay.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDisplay.Location = new System.Drawing.Point(256, 72);
            this.txtDisplay.Name = "txtDisplay";
            this.txtDisplay.Size = new System.Drawing.Size(198, 53);
            this.txtDisplay.TabIndex = 1;
            // 
            // txtComparacion
            // 
            this.txtComparacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtComparacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComparacion.Location = new System.Drawing.Point(256, 148);
            this.txtComparacion.MaxLength = 1;
            this.txtComparacion.Name = "txtComparacion";
            this.txtComparacion.Size = new System.Drawing.Size(198, 53);
            this.txtComparacion.TabIndex = 2;
            // 
            // lblContador
            // 
            this.lblContador.AutoSize = true;
            this.lblContador.Location = new System.Drawing.Point(541, 161);
            this.lblContador.Name = "lblContador";
            this.lblContador.Size = new System.Drawing.Size(74, 17);
            this.lblContador.TabIndex = 3;
            this.lblContador.Text = "Contador: ";
            // 
            // lblContadas
            // 
            this.lblContadas.AutoSize = true;
            this.lblContadas.Location = new System.Drawing.Point(609, 161);
            this.lblContadas.Name = "lblContadas";
            this.lblContadas.Size = new System.Drawing.Size(16, 17);
            this.lblContadas.TabIndex = 4;
            this.lblContadas.Text = "0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lblContadas);
            this.Controls.Add(this.lblContador);
            this.Controls.Add(this.txtComparacion);
            this.Controls.Add(this.txtDisplay);
            this.Controls.Add(this.btnMostrar);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnMostrar;
        private System.Windows.Forms.TextBox txtDisplay;
        private System.Windows.Forms.TextBox txtComparacion;
        private System.Windows.Forms.Label lblContador;
        private System.Windows.Forms.Label lblContadas;
    }
}

