﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laboratorio_Parcial_2
{

	public partial class Form1 : Form
	{
		private int contadorLetras = 0;
		private TextBox txt;
		public Form1()
		{
			InitializeComponent();
		}

		private void btnComenzar_Click(object sender, EventArgs e)
		{
			string cadena = txtDisplay.Text;

			for (int i = 0; i < cadena.Length; i++)
			{
				if (cadena[i] == 'A')
				{
					contadorLetras++;
				}
			}
			if (contadorLetras == 0)
			{
				MessageBox.Show("No hay coincidencias");

			}
			if (contadorLetras > 0)
			{
				MessageBox.Show($"La letra A se repite {contadorLetras} veces");
				contadorLetras = 0;
			}
		}

		private void btnA_Click(object sender, EventArgs e)
		{
			txt.Text += ((Button)sender).Text;
		}

		private void txtDisplay_Enter(object sender, EventArgs e)
		{
			txt = ((TextBox)sender);
		}
	}
}