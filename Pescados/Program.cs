﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pescados
{
	class Program
	{

		static void Main(string[] args)
		{
			//determinar dos mayores

			decimal _pesopescado = 0;
			decimal _primermayor = 0;
			decimal _segundomayor = 0;

			bool _validarDato = false;
			bool _finalizarPrograma = false;
			string _deseaFinalizar = "";

			while(_deseaFinalizar.ToUpper() != "S")
			{
				
				while (_finalizarPrograma == false)
				{
					_validarDato = false;
					while (_validarDato == false)
					{
						Console.Write("Ingrese el peso de los pescados (Presione 0 para finalizar la carga): ");
						if (decimal.TryParse(Console.ReadLine(), out _pesopescado) && _pesopescado >= 0)
						{

							if (_pesopescado == 0)
							{
								_finalizarPrograma = true;
							}
							else if (_pesopescado > _primermayor)
							{
								_segundomayor = _primermayor;
								_primermayor = _pesopescado;
							}
							else
							{
								if (_pesopescado > _segundomayor)
								{
									_segundomayor = _pesopescado;
								}
							}
							_validarDato = true;
						}
					}
				}
				Console.WriteLine("Desea mostrar los resultados?");
				_deseaFinalizar = Console.ReadLine();

			}
			


			//Mostrar resultados

			Console.WriteLine($"El mayor pescado es {_primermayor} y el segundo mayor es {_segundomayor}");

			Console.ReadKey();

		}
	}
}
