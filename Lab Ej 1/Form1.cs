﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_Ej_1
{
    public partial class FormPrincipal : Form
    {
        public FormPrincipal()
        {
            InitializeComponent();
        }

        private void btnMostrar_Click(object sender, EventArgs e)
        {
            //txtDisplay.Text = "Hola";

            //string mensaje = string.Empty; // variable string vacia
            //string mensaje2 = string.Empty;
            //mensaje = txtValor1.Text;
            //mensaje2 = txtValor2.Text;
            //MessageBox.Show($"Hola {mensaje} {mensaje2}");

            MessageBox.Show($"Hola {txtValor1.Text} {txtValor2.Text}");
            //txtValor2.Text = txtValor1.Text;



            //string valor1 = txtValor1.Text; // copiar valor de forma larga
            //txtValor2.Text = valor1;


            //string valor2 = txtValor2.Text;

            //decimal valor1d = decimal.Parse(valor1); // convierte a decimal
            //decimal valor2d = decimal.Parse(valor2);
            //MessageBox.Show((valor1d + valor2d).ToString()); ; // mostrar mensaje es siempre stringa
        }
    }
}
