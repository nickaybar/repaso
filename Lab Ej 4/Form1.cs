﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_Ej_4
{
    public partial class Form1 : Form
    {
        private string palabraUno;
        private string palabraDos;
        private string resultado;
        public Form1()
        {
            InitializeComponent();

        }

        private void btnComparar_Click(object sender, EventArgs e)
        {
            resultado = "";
            palabraUno = txtPalabraUno.Text;
            palabraDos = txtPalabraDos.Text;

            if (palabraUno.Length == palabraDos.Length)
            {
                for (int i = 0; i < palabraUno.Length; i++)
                {

                    if (palabraUno[i] == palabraDos[i])
                    {
                        resultado += palabraUno[i];
                    }
                    else
                    {
                        resultado += "_";
                    }
                }
            }
            else
            {
                MessageBox.Show("Las palabras tienen que ser de la misma longitud");
            }
            txtResultado.Text = resultado;

            //char[] palabra1 = txtPalabraUno.Text.ToCharArray();
            //char[] palabra2 = txtPalabraDos.Text.ToCharArray();
            //char[] resultado = new char[palabra1.Length];


            ////Array.Sort(palabra1);
            ////Array.Sort(palabra2);

            //for (int i = 0; i < palabra1.Length; i++)
            //{
            //    if (palabraUno[i] == palabraDos[i])
            //    {


            //    }
            //}

        }
        private void Limpiar()
        {
            txtPalabraDos.Clear();
            txtPalabraUno.Clear();
            txtResultado.Clear();
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }
    }
}