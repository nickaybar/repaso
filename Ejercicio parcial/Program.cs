﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_parcial
{
	class Program
	{
		static void Main(string[] args)
		{
			//Declarar precios clases
			const decimal PRIMERACLASE = 1000;
			const decimal TURISTA = 400;
			const int CAPACIDADPRIMERA = 4;
			const int CAPACIDADTURISTA = 8;

			//Declarar variables
			int _asientosPrimeraDisponibles = CAPACIDADPRIMERA;
			int _asientosTuristaDisponibles = CAPACIDADTURISTA;
			decimal _porcentajePrimeraVendido = 0.0m;
			decimal _porcentajeTuristaVendido = 0.0m;
			decimal _recaudacionPrimera = 0.0m;
			decimal _recaudacionTurista = 0.0m;
			int _contadorPrimera = 0;
			int _contadorTurista = 0;
			int _opcion = 0;
			decimal _recaudacionTotal = 0;

			bool _finalizarPrograma = false;
			bool _verificarDato = false;


			//Operacion
			while (_finalizarPrograma == false)
			{
				_verificarDato = false;
				
				while (_verificarDato == false)
				{
					Console.Clear();
					Console.Write($"Ingrese la opcion que quiera ejecutar: \n1- Primera clase (Cantidad disponible: {_asientosPrimeraDisponibles})\n2- Turista (Cantidad disponible: {_asientosTuristaDisponibles})\n3- Devolver Primera\n4- Devolver turista\n0- Mostrar resultados\n\nOpcion: ");
					if (int.TryParse(Console.ReadLine(), out _opcion) && _opcion >= 0 && _opcion <= 4)
					{
						if (_opcion == 1)
						{
							if (_asientosPrimeraDisponibles > 0)
							{
								_contadorPrimera++;
								_recaudacionPrimera = PRIMERACLASE * _contadorPrimera;
								_asientosPrimeraDisponibles--;
							}
						}
						if (_opcion == 2)
						{
							if (_asientosTuristaDisponibles > 0)
							{
								_contadorTurista++;
								_recaudacionTurista = TURISTA * _contadorTurista;
								_asientosTuristaDisponibles--;
							}	
						}
						if (_opcion == 3)
						{
							if (_contadorPrimera > 0)
							{
								_contadorPrimera--;
								_recaudacionPrimera = (PRIMERACLASE * _contadorPrimera);
								_asientosPrimeraDisponibles++;
							}
						}
						if (_opcion == 4)
						{
							if (_contadorTurista > 0)
							{
								_contadorTurista--;
								_recaudacionTurista = (TURISTA * _contadorTurista);
								_asientosTuristaDisponibles++;
							}

						}
						if (_opcion == 0)
						{
							_finalizarPrograma = true;
						}
						_verificarDato = true;
					}
					else
					{
						Console.WriteLine("Ingreso una opcion incorrecta. Por favor intente de nuevo");
						Console.ReadKey();
					}
				}
			}
			//Procesar
			_recaudacionTotal = _recaudacionPrimera + _recaudacionTurista;
			_porcentajePrimeraVendido = (decimal)(_contadorPrimera * 100) / CAPACIDADPRIMERA;
			_porcentajeTuristaVendido = (decimal)(_contadorTurista * 100) / CAPACIDADTURISTA;

			// Mostrar resultados
			Console.WriteLine();
			Console.WriteLine($"El total de dinero recaudado en primera es ${_recaudacionPrimera}");
			Console.WriteLine($"El total de dinero recaudado en turista es ${_recaudacionTurista}");
			Console.WriteLine($"La recaudacion total entre ambas clases es ${_recaudacionTotal}");
			Console.WriteLine($"La cantidad de asientos vendidos en primera es {_contadorPrimera}");
			Console.WriteLine($"La cantidad de asientos vendidos en turista es {_contadorTurista}");
			Console.WriteLine($"El porcentaje vendido en primera es de {_porcentajePrimeraVendido}%");
			Console.WriteLine($"El porcentaje vendido en turista es de {_porcentajeTuristaVendido}%");
			Console.ReadKey();
		}
	}
}