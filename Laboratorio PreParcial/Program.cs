﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratorio_PreParcial
{
	class Program
	{
		static void Main(string[] args)
		{
			// Constantes multas

			const int SEMAFOROROJO = 3500;
			const int MALESTACIONADO = 1200;
			const int EXCESOVELOCIDAD = 3000;
			const int FALTAPAPELES = 800;

			// Declarar varibles a ingresar

			int _codigoInfraccion = 0;
			decimal _porcentajeRojo = 0;
			int _contadorGeneral = 0;
			int _contadorRojo = 0;
			bool _finalizarPrograma = false;
			bool _verificarDato = false;

			
			
			while (_finalizarPrograma == false)
			{	
				
				Console.Clear();
				Console.WriteLine("TIPOS DE INFRACCION\n1- SEMAFORO ROJO\n2- MAL ESTACIONADO\n3- EXCESO DE VELOCIDAD\n4- FALTA PAPELES\n0- SALIR");
				Console.WriteLine();
				Console.Write("Opcion: ");
				_verificarDato = false;
				while (_verificarDato == false)
				{
					if (int.TryParse(Console.ReadLine(), out _codigoInfraccion))
					{
						switch (_codigoInfraccion)
						{
							case 1:
								_contadorRojo++;
								_contadorGeneral++;
								break;
							case 2:
								_contadorGeneral++;
								break;
							case 3:
								_contadorGeneral++;
								break;
							case 4:
								_contadorGeneral++;
								break;
							case 0:
								_finalizarPrograma = true;
								break;
						}
						_verificarDato = true;
					}
				}
			}
			
			//Operacion
			_porcentajeRojo = _contadorRojo * 100 / _contadorGeneral;

			//Mostrar resultados
			Console.WriteLine($"El porcentaje de autos que pasaron en rojo fue: {_porcentajeRojo}%");
			Console.ReadKey();
		}
	}
}
