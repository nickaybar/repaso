﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            const int ELEMENTOS = 5; // Una constanste se debe declarar en mayus por buena practica.
            int[] _vec = new int[ELEMENTOS]; // Vec es la variable estructurada. int[array unidimensional]. // new int[CANTIDAD DE ENTEROS]
            int _numero = 0;
            bool _encontrado = false;
            Random _generador = new Random();

            Console.Clear();


            // cargar el vector de forma aleatoria
            for (int _i = 0; _i < _vec.Length; _i++) // _veclength devuelve la cantidad de ese array. en este caso del array _vec
            {
                _vec[_i] = _generador.Next(100);
            }
            // mostrar el vector 

            for (int _i = 0; _i < ELEMENTOS; _i++)
            {
                Console.Write($"{_vec[_i]} "); // muestra el vector con variable iniciada _vec[_i]
            }

            Console.WriteLine();
            Console.WriteLine("Ingrese un numero:");
            _numero = Convert.ToInt32(Console.ReadLine());

            // Buscar elemento
            _encontrado = false; // bandera bo
            for (int _i = 0; _i < _vec.Length; _i++)
            {
                if(_numero == _vec[_i])
                {

                    _encontrado = true;
                }
                else
                {
                    
                }
                
            }
            if (_encontrado == true)
            {
                Console.WriteLine();
                Console.WriteLine($"El numero {_numero} si pertenece al vector");
            }
                
            else
            {
                Console.WriteLine("El numero no pertenece al vector");
            }
            Console.ReadKey();
        }
    }
}
