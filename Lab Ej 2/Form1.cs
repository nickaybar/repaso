﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_Ej_2
{
    public partial class Form1 : Form
    {
        private string operador;
        private int valor1;
        private int valor2;
        private int resultado;
        public Form1()
        {
            InitializeComponent();
            operador = string.Empty; // es lo mismo que hacer operador = "";
            valor1 = 0;
            valor2 = 0;
        }
        private string RealizarOperacion(int valor1, int valor2, string operador)
        {
            resultado = 0;

            switch (operador)
            {
                case "+":
                    resultado = valor1 + valor2;
                    break;
            }

            return resultado.ToString();

        }

        private void btn1_Click(object sender, EventArgs e)
        {
            txtDisplay.Text += ((Button)sender).Text;
        }

        private void btnSuma_Click(object sender, EventArgs e)
        {
 
            valor1 = int.Parse(txtDisplay.Text);
            txtDisplay.Clear();
            operador = "+";

        }

    
        private void btnIgual_Click(object sender, EventArgs e)
        {
            valor2 = int.Parse(txtDisplay.Text);
            txtDisplay.Clear();
            txtDisplay.Text = RealizarOperacion(valor1, valor2, operador);
            //resultado = int.Parse(txtDisplay.Text);
            
            
        }

        
    }
}
