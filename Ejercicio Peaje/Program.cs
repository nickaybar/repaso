﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_Peaje
{
    class Program
    {
        static void Main(string[] args)
        {
            // Declaracion cantidad de vehiculos

            int _seleccionador = 0;

            // Declaracion Precios
            decimal _precioAutos = 35m;
            decimal _precioMotos = 15m;
            decimal _precioCamionetas = 45m;
            decimal _precioCamiones = 80m;

            // Declaracion Totales
            decimal _recaudacion = 0m;

            // Contador tickets
            int _contadorAutos = 0;
            int _contadorMotos = 0;
            int _contadorCamiones = 0;
            int _contadorCamionetas = 0;

            // While
            string _deseaAgregar = "S";

            while (_deseaAgregar.ToUpper() == "S")
            {
                Console.Clear();
                Console.WriteLine("Seleccione el tipo de vehiculo que acaba de ingresar");
                Console.WriteLine("[1- Motos | 2- Autos | 3- Camionetas | 4- Camiones]");
                if (int.TryParse(Console.ReadLine(), out _seleccionador) && (_seleccionador > 0 && _seleccionador <= 4))
                {
                    if (_seleccionador == 1) // motos
                    {
                        _contadorMotos++;
                        //_recaudacion =  _precioMotos * _contadorMotos; //
                       _recaudacion += _precioMotos;
                    }
                    else if (_seleccionador == 2) // autos
                    {
                        _contadorAutos++;
                        _recaudacion += _precioAutos;
                    }
                    else if (_seleccionador == 3) // camionestas
                    {
                        _contadorCamionetas++;
                        _recaudacion += _precioCamionetas;
                    }
                    else if (_seleccionador == 4)// camiones
                    {
                        _contadorCamiones++;
                        _recaudacion += _precioCamiones;
                    }
                }

                Console.WriteLine();
                Console.WriteLine("Desea registrar otro vehiculo? [S para SI - N para NO]");

                _deseaAgregar = Console.ReadLine();

                while ((_deseaAgregar.ToUpper() != "S") && ( _deseaAgregar.ToUpper() != "N"))
                {

                    Console.WriteLine("Error. Ingrese solo S o N");
                    _deseaAgregar = Console.ReadLine();
                }

            }
            // Mostrar resultados
            Console.WriteLine($"La cantidad recaudada es de ${_recaudacion}.\nLa cantidad de camionetas que ingresaron fue de {_contadorCamionetas}");
            Console.WriteLine($"La cantidad de autos que ingresaron equivale al {(((_contadorAutos * _precioAutos) / _recaudacion ) * 100).ToString("0.00")}%");
            Console.ReadKey();
        }
    }
}

