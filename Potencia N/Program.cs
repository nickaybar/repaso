﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Potencia_N
{
	class Program
	{
		static void Main(string[] args)
		{
			int _numero = 0;
			int _exponente = 0;
			int _resultado = 1;

			Console.Write("Ingrese el numero base: ");
			if (int.TryParse(Console.ReadLine(), out _numero))
			{
				Console.Write("Ingrese el exponente: ");
				if (int.TryParse(Console.ReadLine(), out _exponente))
				{
					for (int _i = 1; _i <= _exponente; _i++)
					{
						_resultado = _resultado * _numero;
					}
				}
			}
				
			Console.WriteLine($"El resultado de 2 elevado al {_exponente} es {_resultado}");
			Console.ReadKey();


		}
	}
}
