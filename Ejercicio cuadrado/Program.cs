﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_cuadrado
{
	class Program
	{
		static void Main(string[] args)
		{

			for(int _i = 0; _i <= 100; _i++)
			{
				Console.WriteLine($"El cuadrado del numero {_i} es {_i * _i}");
			}
			Console.ReadKey();
		}
	}
}
