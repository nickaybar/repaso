﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_Taxi
{
    class Program
    {
        static void Main(string[] args)
        {
            const double _BAJADADEBANDERA = 17.00;
            const double _VALORFICHA = 1.70;


            // distancia en metros
            double _distanciaRecorrida = 0.0;
            double _montoAPagar = 0.0;
            string _deseaCalcular = "S";
            bool _centinela = false;

            while(_deseaCalcular.ToUpper() == "S")
            {
                _centinela = false;
                while(_centinela == false)
                {
                    Console.Clear();
                    Console.Write("Ingrese la distancia expresada en metros: ");
                    if (double.TryParse(Console.ReadLine(), out _distanciaRecorrida) && _distanciaRecorrida > 0)
                    {
                        _montoAPagar = _BAJADADEBANDERA + (_VALORFICHA * _distanciaRecorrida);
                        _centinela = true;
                        Console.WriteLine($"El monto a pagar es ${_montoAPagar}");
                    }
                    else
                    {
                        Console.WriteLine("Ocurrio un error. Por favor ingrese la distancia expresada en metros");
                        _centinela = false;
                    }
                    Console.WriteLine();
                    Console.WriteLine("Desea calcular otro viaje? [S para SI - N para NO]");
                    _deseaCalcular = Console.ReadLine();
                    Console.WriteLine("Presione cualquier tecla para finalizar...");
                }
               
            }
            Console.ReadKey();
        }
    }
}
