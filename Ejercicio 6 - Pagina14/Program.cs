﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_6___Pagina14
{
    class Program
    {
        static void Main(string[] args)
        {
            // Declarar
            decimal _numero = 0;
            int _contador = 0;
            bool _bandera = false;

            Console.WriteLine("Ingrese la cantidad de numeros que usted quiera, para finalizar ingrese un numero negativo. ");
            while (_bandera == false)
            {
                
                Console.Write("Ingrese un numero positivo: ");
                if (decimal.TryParse(Console.ReadLine(), out _numero))
                {
                    if (_numero < 0)
                    {
                        _bandera = true;
                    }
                    else
                    {
                        if(_numero == 8)
                        {
                            _contador++;
                        }
                        
                    }
                }
                else
                {
                    Console.WriteLine("Ocurrio un error. Por favor ingrese un numero positivo.");
                }
            }
            // Mostrar resultado
            Console.WriteLine($"El numero 8 se ingreso {_contador} veces");
            Console.ReadKey();
        }
    }
}
