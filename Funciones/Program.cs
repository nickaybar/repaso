﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Funciones
{
    class Program
    {
        //Funciones sin tipo (void)
        //y sin parametros o argumentos


        public static void Creditos() // Funcion vacia, no retorna nada. Se la puede usar en una linea nueva de codigo. Estas no tienen ni tipo ni parametros (VOID)
        {
            Console.Clear();

            Console.WriteLine("UTN - FRI");
            Console.WriteLine("LABORATORIO I");
            Console.WriteLine("2018");

            Console.ReadKey();
        }


        // static int. Funcion que retorna algun dato de tipo entero ( STATIC INT ) . 
        // Hay que agregarle parametros, en este caso _a ; _b
        // Funciones con tipo, y con parametros.

        public static int SumarEnteros(int _a, int _b)  // un solo punto de entrada // dos parametros de entradas.
        {
            int _resultado = 0; // Resultado

            _resultado = _a + _b; // Sumando las variables declaradas

            return _resultado; // Un solo punto de salida
        }

        public static bool EsImpar(int _numero) // Funcion para saber si el numero es impar
        {
            bool _resultado = false; // Booleano 

            if (_numero % 2 == 1)
            {
                _resultado = true;
            }

            else
            {
                _resultado = false;
            }

            return _resultado;
        }


        static void Main(string[] args) // Principal cuerpo del programa
        {
            int _sumando1 = 0;
            int _sumando2 = 0;
            int _total = 0;

            bool _res = false;
            Console.Clear();

            Console.Write("Sumando 1: ");
            _sumando1 = Convert.ToInt32(Console.ReadLine());

            Console.Write("Sumando 2: ");
            _sumando2 = Convert.ToInt32(Console.ReadLine());


            _total = SumarEnteros(_sumando1, _sumando2); // Le asigna a la funcion 'SumarEnteros' las variables _sumando1 a 'a' y _sumando2 a 'b'

            Console.WriteLine($"Total vale: {_total}");

            Console.WriteLine();

            _res = EsImpar(_sumando1);

            if (_res) // se la entiende a la sentencia como true
            {
                Console.WriteLine("El numero es par");
            }
            else // fals
            {
                Console.WriteLine("El numero es impar");
            }

            Console.ReadKey();

            Creditos();

            Console.ReadKey();
        }
    }
}