﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_YPF
{
	class Program
	{
		public static void MayorVenta(decimal[] vector)
		{
			decimal _mayor = int.MinValue;

			for (int i = 0; i < vector.Length; i++)
			{
				if(vector[i] > _mayor)
				{
					_mayor = vector[i];
				}
			}
			Console.WriteLine($"La mayor venta fue de {_mayor} litros");
		}

		public static void MenorVenta(decimal[] vector, int dias)
		{
			decimal _menor = int.MaxValue;

			for (int i = 0; i < dias; i++)
			{
				if (vector[i] < _menor)
				{
					_menor = vector[i];
				}
			}
			Console.WriteLine($"La menor venta fue de {_menor} litros");
		}


		static void Main(string[] args)
		{
			//Declaracion constantes y vectores
			const decimal PRECIONAFTA = 29.6m;
			decimal _dias = 0;
			decimal[] vec = new decimal[_dias];

			//Declaracion variables de entrada
			decimal _litrosVendidos = 0.0m;
		
			//Variables de proceso y salida
			decimal _recaudacionTotal = 0.0m;
			decimal _promedio = 0.0m;
			decimal _cantidadLitrosVendida = 0.0m;
			

			//Variables de ciclo
			bool _verificarDato = false;
			bool _finalizarPrograma = false;


			//////////////

			while (_finalizarPrograma == false)
			{

				_verificarDato = false;
				while (_verificarDato==false)
				{
					Console.Write("Ingrese la cantidad de dias que quiera realizar la carga de litros vendidos: ");
					if (decimal.TryParse(Console.ReadLine(), out _dias) && _dias > 0)
					{
						_verificarDato = true;
					} 
				}

				_verificarDato = false;
				while (_verificarDato==false)
				{
					Console.WriteLine("A continuacion, indique cuanto se vendio en cada dia");
					Console.WriteLine();
					for (int i = 0; i < _dias; i++)
					{
						Console.Write($"Indique los litros vendidos en el dia {i + 1}: " );
						if (decimal.TryParse(Console.ReadLine(), out _litrosVendidos) && _litrosVendidos > 0)
						{
							vec[i] = _litrosVendidos;
							_cantidadLitrosVendida += _litrosVendidos; // Sumatoria cantidad litros vendidos
							_verificarDato = true;
						}
						_finalizarPrograma = true;
					} 
				}
			}
			
			//Operaciones
			_promedio = _cantidadLitrosVendida / _dias;
			_recaudacionTotal = _cantidadLitrosVendida * PRECIONAFTA;


			//Mostrar resultados
			Console.WriteLine();
			Console.WriteLine($"----RESULTADOS----");
			Console.WriteLine($"Se vendio un total de {_cantidadLitrosVendida} litros de nafta");
			Console.WriteLine($"La cantidad vendida representa una suma de ${_recaudacionTotal}");
			Console.WriteLine($"El promedio de litros vendidos en los {_dias} dias fue de {_promedio}");
			Console.WriteLine("\nPresione cualquier tecla para finalizar...");
			MayorVenta(vec);
			MenorVenta(vec, _dias);
			Console.ReadKey();
		}
	}
}
