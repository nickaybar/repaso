﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio1
{
    class Program
    {
        static void Main(string[] args)
        {
            // declarar los dias
            decimal _nivel = 0.0m;
            decimal _promedio = 0.0m;
            decimal _suma = 0.0m;

            int _diaMaximoNivel = 0;
            decimal _maximoNivel = -10000.0m;
            int _diaMinimoNivel = 0;
            decimal _minimoNivel = 10000.0m;

            // Operar 

            Console.Clear();

            //Carga de niveles
            _suma = 0;
            for (int _i = 1; _i <= 7; _i++)
            {
                Console.Write($"Nivel dia {_i}: ");
                _nivel = Convert.ToDecimal(Console.ReadLine());

                if (_nivel > _maximoNivel)
                {
                    _maximoNivel = _nivel; // nuevo maximo nivel
                    _diaMaximoNivel = _i; // el dia esta almacenado en el _i
                }
                if (_nivel < _minimoNivel)
                {
                    _minimoNivel = _nivel;
                    _diaMinimoNivel = _i;

                }
                _suma = _suma + _nivel;
               
            }
            _promedio = _suma / 7;
            Console.WriteLine();
            Console.Write($"El maximo nivel fue {_maximoNivel}, alcanzado el ");

            switch (_diaMaximoNivel)
            {
                case 1: { Console.WriteLine("Lunes");  break;};
                case 2: { Console.WriteLine("Martes"); break; };
                case 3: { Console.WriteLine("Miercoles"); break; };
                case 4: { Console.WriteLine("Jueves"); break; };
                case 5: { Console.WriteLine("Viernes"); break; };
                case 6: { Console.WriteLine("Sabado"); break; };
                case 7: { Console.WriteLine("Domingo"); break; };
            }

            Console.WriteLine();
            Console.Write($"El minimo nivel fue {_minimoNivel}, alcanzado el ");

            switch (_diaMinimoNivel)
            {
                case 1: { Console.WriteLine("Lunes"); break; };
                case 2: { Console.WriteLine("Martes"); break; };
                case 3: { Console.WriteLine("Miercoles"); break; };
                case 4: { Console.WriteLine("Jueves"); break; };
                case 5: { Console.WriteLine("Viernes"); break; };
                case 6: { Console.WriteLine("Sabado"); break; };
                case 7: { Console.WriteLine("Domingo"); break; };
            }
            Console.WriteLine($"El nivel promedio fue de {decimal.Round(_promedio, 2)} litros");
            Console.ReadKey();
        }
    }
}