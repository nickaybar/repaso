﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CajeroAutomatico
{
    class Program
    {
        public static float ValorPI() // Funcion de tipo float sin parametros
        {
            float _resultado = 3.14f;

            return _resultado;
        }
        
        public static void Saludar(string _nombre) // Funcion de tipo vacio con parametros
        {
            Console.WriteLine("Hola " + _nombre);
        }

        public static int Menu()// Funcion de tipo entero sin parametros
        {
            int _opcion = 0;
            Console.Clear();
            Console.WriteLine("MENU");
            Console.WriteLine("1- Deposito");
            Console.WriteLine("2- Extraccion");
            Console.WriteLine("3- Saldo");
            Console.WriteLine("4- Salir");
            Console.WriteLine();
            Console.Write("Opcion: ");
            _opcion = Convert.ToInt32(Console.ReadLine());
            return _opcion;
        }


        static void Main(string[] args)
        {
            decimal _saldo = 0.0m;
            decimal _monto = 0.0m;

            Console.Clear();

            /*           Saludar("Pepe"); // Llama la funcion Saludar
                       Saludar("Juan"); // llama la funcion saludar

                       Console.WriteLine($"Valor de Pi: " + ValorPI().ToString()); // Llamando a la funcion ValorPI()
                       Console.WriteLine();

           */
            int _opc = 0;
            _saldo = 1500.00m;

            Console.Clear();

            do
            {
                _opc = Menu(); // Llamamos a la funcion Menu
                Console.Clear();
                switch (_opc)
                {
                    case 2:
                        {
                            Console.WriteLine("EXTRACCION");
                            Console.WriteLine();
                            Console.WriteLine($"Su saldo actual es de {_saldo}");
                            Console.Write("Ingrese el monto que desee extraer: ");

                            _monto = Convert.ToDecimal(Console.ReadLine());
                            if ((_monto > 0) && (_monto <= _saldo))
                            {
                                _saldo = _saldo - _monto;
                                Console.WriteLine($"La extraccion se hizo correctamente. Su saldo actual es de ${_saldo}");

                            }
                            else
                            {
                                Console.WriteLine("Error al extraer.");
                            }

                            break;

                        }
                    case 1:
                        {
                            Console.WriteLine("DEPOSITO");
                            Console.WriteLine();
                            Console.WriteLine($"Su saldo actual es de ${_saldo}");
                            Console.Write("Ingrese el Monto que desee depositar: ");

                            _monto = Convert.ToDecimal(Console.ReadLine());
                            if (_monto > 0)
                            {
                                
                                _saldo = _saldo + _monto;
                                Console.WriteLine($"El nuevo Saldo total es de ${_saldo}");
                            }
                            else
                            {
                                Console.WriteLine("Error. Ingrese un monto positivo");
                            }
                            
                            break;
                        }
                    case 3:
                        {
                            Console.WriteLine("SALDO");
                            Console.WriteLine($"Su saldo actual es de ${_saldo}");
                            break;
                        }
                    case 4:
                        {
                            Console.WriteLine("SALIO DEL SISTEMA");
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("OPCION INCORRECTA");
                            break;
                        }
                };

                Console.ReadKey();
            } while (_opc != 4);




            Console.ReadKey();
        }
    }
}
