﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Juego_Matria
{
	class Program
	{
		public static void CargarMatriz(int[,] mat) // carga la matriz, funcion sin retorno
		{
			int numero = 1;

			for (int _f = 0; _f < mat.GetLength(0); _f++) //mat getlenght (0) hace referencia al numero de filas
			{
				for (int _c = 0; _c < mat.GetLength(1); _c++) // (1) hace referencia al numero de columnas
				{
					mat[_f, _c] = numero++;
				} //for c
			}//for f
		}

		public static void MostrarMatriz(int[,] mat) // muestra la matriz

		{
			for (int _f = 0; _f < mat.GetLength(0); _f++) //mat getlenght (0) hace referencia al numero de filas
			{
				for (int _c = 0; _c < mat.GetLength(1); _c++) // (1) hace referencia al numero de columnas
				{
					Console.Write("{0} ", mat[_f, _c].ToString("00"));
					System.Threading.Thread.Sleep(20); // comando para agregarle retraso ahre
				} //for c
				Console.WriteLine();
				System.Threading.Thread.Sleep(20); // comando para agregarle retraso ahre
			}//for f
		}

		public static void MezclarMatriz(int[,] mat) // TA BIEN PILLO ESTE SHUFFLE
		{
			Random generador = new Random();
			int veces = 0;
			int colA = 0;
			int filA = 0;
			int colB = 0;
			int filB = 0;
			int aux = 0;


			for (veces = 1; veces < 1000; veces++)
			{
				filA = generador.Next(0, mat.GetLength(0));
				colA = generador.Next(0, mat.GetLength(1));

				filB = generador.Next(0, mat.GetLength(0));
				colB = generador.Next(0, mat.GetLength(1));

				aux = mat[filA, colA];
				mat[filA, colA] = mat[filB, colB];
				mat[filB, colB] = aux;
			}
		}


		public static void CargarMatrizParejas(int[,] mat) // carga la matriz, funcion sin retorno
		{
			int numero = 1;
			int veces = 0;
			for (int _f = 0; _f < mat.GetLength(0); _f++) //mat getlenght (0) hace referencia al numero de filas
			{
				for (int _c = 0; _c < mat.GetLength(1); _c++) // (1) hace referencia al numero de columnas
				{
					mat[_f, _c] = numero;
					veces++;
					if (veces == 2)
					{
						veces = 0;
						numero++;
					}
				} //for c
			}//for f
		}



		static void Main(string[] args)
		{
			const int FILAS = 4;
			const int COLUMNAS = 4;

			int[,] vec = new int[FILAS, COLUMNAS];

			CargarMatriz(vec);
			MostrarMatriz(vec);
			Console.WriteLine();
			MezclarMatriz(vec);
			MostrarMatriz(vec);
			Console.WriteLine();
			CargarMatrizParejas(vec);
			MostrarMatriz(vec);
			Console.ReadKey();
		}
	}
}