﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratorio_PreParcial2
{
	class Program
	{
		static void Main(string[] args)
		{
			// Censo poblacion

			int _contadorAgua = 0;
			int _contadorCable = 0;
			int _contadorFamilia = 0;
			int _opcion = 0;
			decimal _promedioAgua = 0;
			decimal _promedioCable = 0;
			bool _finalizarPrograma = false;
			bool _verificarDatos = false;
			//string _deseaCensar = "S";

			while (_finalizarPrograma == false)
			{
				_verificarDatos = false;
				while (_verificarDatos == false)
				{
					Console.Clear();
					Console.Write("Desea consultar a una familia sus servicios? [1 para SI - 0 para NO]: ");
					if (int.TryParse(Console.ReadLine(), out _opcion) && _opcion == 1 || _opcion == 0)
					{
						_verificarDatos = true;
					}
				}

				if (_opcion == 1)
				{
					_contadorFamilia++;

					_verificarDatos = false;
					while (_verificarDatos == false)
					{
						Console.Clear();
						Console.WriteLine("1- Tiene agua?\n2- Tiene cable?\n3- Tiene luz?\n4- Tiene gas?\n5- Tiene cloacas\n6- Tiene telefono\n7- Tiene internet?\n8- Consultar otra familia\n0-Salir");
						Console.Write("\nOpcion: ");
						_verificarDatos = false;
						if (int.TryParse(Console.ReadLine(), out _opcion))
						{
							switch (_opcion)
							{
								case 1:
									_contadorAgua++;
									break;
								case 2:
									_contadorCable++;
									break;

								case 8:
									_contadorFamilia++;
									break;
								case 0:
									_finalizarPrograma = true;
									_verificarDatos = true;
									break;

							}
						}
					}
				}
			}

			//Procesar operaciones
			_promedioAgua = _contadorAgua * 100 / _contadorFamilia;
			_promedioCable = _contadorCable * 100 / _contadorFamilia;

			//Mostrar resultados
			Console.WriteLine($"El promedio de familias consultadas ({_contadorFamilia}) con agua es: {_promedioAgua}% ");
			Console.WriteLine($"El promedio de familias consultadas ({_contadorFamilia}) con agua es: {_promedioCable}% ");
			Console.ReadKey();
		}
	}
}