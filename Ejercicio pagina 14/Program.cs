﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_pagina_14
{
    class Program
    {
        static void Main(string[] args)
        {
            ///////////////////////////

            int _valor = 0;
            int _contador = 0;
            int _contadorMil = 0;
            int _contadorCien = 0;
            int _contadorDiez = 0;

            ///////////////////////////


            Console.WriteLine("Por favor ingrese un numero de 4 cifras");

            if (int.TryParse(Console.ReadLine(), out _valor))
            {
                while (_valor >= 1000) // haga mientras
                {
                    _valor = _valor - 1000;
                    _contadorMil++;
                }
                while (_valor >= 100) // haga mientras
                {
                    _valor = _valor - 100;
                    _contadorCien++;
                }
                while (_valor >= 10) // haga mientras
                {
                    _valor = _valor - 10;
                    _contadorDiez++;
                }

                _contador = _valor;
            }
            Console.WriteLine($"Resultado = ({_contadorMil + _contadorCien + _contadorDiez + _contador})");

            Console.ReadKey();
        }
    }
}
