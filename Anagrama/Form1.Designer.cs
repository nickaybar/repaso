﻿namespace Anagrama
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtOrigen = new System.Windows.Forms.TextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.txtDestino = new System.Windows.Forms.TextBox();
            this.lblPalabraUno = new System.Windows.Forms.Label();
            this.lblPalabraDos = new System.Windows.Forms.Label();
            this.txtResultado = new System.Windows.Forms.TextBox();
            this.lblResultado = new System.Windows.Forms.Label();
            this.lblCoincidencias = new System.Windows.Forms.Label();
            this.lblAciertos = new System.Windows.Forms.Label();
            this.btnBorrar = new System.Windows.Forms.Button();
            this.BtnReiniciar = new System.Windows.Forms.Button();
            this.btnSet = new System.Windows.Forms.Button();
            this.btnSet2 = new System.Windows.Forms.Button();
            this.btnA = new System.Windows.Forms.Button();
            this.btnB = new System.Windows.Forms.Button();
            this.btnC = new System.Windows.Forms.Button();
            this.btnD = new System.Windows.Forms.Button();
            this.btnMostrar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtOrigen
            // 
            this.txtOrigen.Enabled = false;
            this.txtOrigen.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOrigen.Location = new System.Drawing.Point(271, 100);
            this.txtOrigen.Name = "txtOrigen";
            this.txtOrigen.Size = new System.Drawing.Size(263, 45);
            this.txtOrigen.TabIndex = 0;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // txtDestino
            // 
            this.txtDestino.Enabled = false;
            this.txtDestino.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDestino.Location = new System.Drawing.Point(271, 168);
            this.txtDestino.Name = "txtDestino";
            this.txtDestino.Size = new System.Drawing.Size(263, 45);
            this.txtDestino.TabIndex = 2;
            // 
            // lblPalabraUno
            // 
            this.lblPalabraUno.AutoSize = true;
            this.lblPalabraUno.Location = new System.Drawing.Point(146, 110);
            this.lblPalabraUno.Name = "lblPalabraUno";
            this.lblPalabraUno.Size = new System.Drawing.Size(73, 17);
            this.lblPalabraUno.TabIndex = 4;
            this.lblPalabraUno.Text = "Palabra 1:";
            // 
            // lblPalabraDos
            // 
            this.lblPalabraDos.AutoSize = true;
            this.lblPalabraDos.Location = new System.Drawing.Point(146, 180);
            this.lblPalabraDos.Name = "lblPalabraDos";
            this.lblPalabraDos.Size = new System.Drawing.Size(73, 17);
            this.lblPalabraDos.TabIndex = 5;
            this.lblPalabraDos.Text = "Palabra 2:";
            // 
            // txtResultado
            // 
            this.txtResultado.Enabled = false;
            this.txtResultado.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResultado.Location = new System.Drawing.Point(271, 241);
            this.txtResultado.Name = "txtResultado";
            this.txtResultado.Size = new System.Drawing.Size(263, 45);
            this.txtResultado.TabIndex = 6;
            // 
            // lblResultado
            // 
            this.lblResultado.AutoSize = true;
            this.lblResultado.Location = new System.Drawing.Point(146, 262);
            this.lblResultado.Name = "lblResultado";
            this.lblResultado.Size = new System.Drawing.Size(76, 17);
            this.lblResultado.TabIndex = 7;
            this.lblResultado.Text = "Resultado:";
            // 
            // lblCoincidencias
            // 
            this.lblCoincidencias.AutoSize = true;
            this.lblCoincidencias.Location = new System.Drawing.Point(295, 298);
            this.lblCoincidencias.Name = "lblCoincidencias";
            this.lblCoincidencias.Size = new System.Drawing.Size(99, 17);
            this.lblCoincidencias.TabIndex = 8;
            this.lblCoincidencias.Text = "Coincidencias:";
            // 
            // lblAciertos
            // 
            this.lblAciertos.AutoSize = true;
            this.lblAciertos.Location = new System.Drawing.Point(409, 298);
            this.lblAciertos.Name = "lblAciertos";
            this.lblAciertos.Size = new System.Drawing.Size(16, 17);
            this.lblAciertos.TabIndex = 9;
            this.lblAciertos.Text = "0";
            // 
            // btnBorrar
            // 
            this.btnBorrar.Location = new System.Drawing.Point(2, 332);
            this.btnBorrar.Name = "btnBorrar";
            this.btnBorrar.Size = new System.Drawing.Size(263, 45);
            this.btnBorrar.TabIndex = 10;
            this.btnBorrar.Text = "Borrar";
            this.btnBorrar.UseVisualStyleBackColor = true;
            this.btnBorrar.Click += new System.EventHandler(this.btnBorrar_Click);
            // 
            // BtnReiniciar
            // 
            this.BtnReiniciar.Location = new System.Drawing.Point(540, 332);
            this.BtnReiniciar.Name = "BtnReiniciar";
            this.BtnReiniciar.Size = new System.Drawing.Size(263, 45);
            this.BtnReiniciar.TabIndex = 11;
            this.BtnReiniciar.Text = "Reiniciar";
            this.BtnReiniciar.UseVisualStyleBackColor = true;
            this.BtnReiniciar.Click += new System.EventHandler(this.BtnReiniciar_Click);
            // 
            // btnSet
            // 
            this.btnSet.Location = new System.Drawing.Point(540, 100);
            this.btnSet.Name = "btnSet";
            this.btnSet.Size = new System.Drawing.Size(164, 45);
            this.btnSet.TabIndex = 12;
            this.btnSet.Text = "Set";
            this.btnSet.UseVisualStyleBackColor = true;
            this.btnSet.Click += new System.EventHandler(this.btnSet_Click);
            // 
            // btnSet2
            // 
            this.btnSet2.Location = new System.Drawing.Point(540, 168);
            this.btnSet2.Name = "btnSet2";
            this.btnSet2.Size = new System.Drawing.Size(164, 45);
            this.btnSet2.TabIndex = 13;
            this.btnSet2.Text = "Set";
            this.btnSet2.UseVisualStyleBackColor = true;
            this.btnSet2.Click += new System.EventHandler(this.btnSet2_Click);
            // 
            // btnA
            // 
            this.btnA.Location = new System.Drawing.Point(268, 385);
            this.btnA.Name = "btnA";
            this.btnA.Size = new System.Drawing.Size(61, 53);
            this.btnA.TabIndex = 14;
            this.btnA.Text = "A";
            this.btnA.UseVisualStyleBackColor = true;
            this.btnA.Click += new System.EventHandler(this.btnA_Click);
            // 
            // btnB
            // 
            this.btnB.Location = new System.Drawing.Point(335, 385);
            this.btnB.Name = "btnB";
            this.btnB.Size = new System.Drawing.Size(61, 53);
            this.btnB.TabIndex = 15;
            this.btnB.Text = "B";
            this.btnB.UseVisualStyleBackColor = true;
            this.btnB.Click += new System.EventHandler(this.btnA_Click);
            // 
            // btnC
            // 
            this.btnC.Location = new System.Drawing.Point(402, 385);
            this.btnC.Name = "btnC";
            this.btnC.Size = new System.Drawing.Size(61, 53);
            this.btnC.TabIndex = 16;
            this.btnC.Text = "C";
            this.btnC.UseVisualStyleBackColor = true;
            this.btnC.Click += new System.EventHandler(this.btnA_Click);
            // 
            // btnD
            // 
            this.btnD.Location = new System.Drawing.Point(469, 385);
            this.btnD.Name = "btnD";
            this.btnD.Size = new System.Drawing.Size(61, 53);
            this.btnD.TabIndex = 17;
            this.btnD.Text = "D";
            this.btnD.UseVisualStyleBackColor = true;
            this.btnD.Click += new System.EventHandler(this.btnA_Click);
            // 
            // btnMostrar
            // 
            this.btnMostrar.Location = new System.Drawing.Point(271, 332);
            this.btnMostrar.Name = "btnMostrar";
            this.btnMostrar.Size = new System.Drawing.Size(263, 45);
            this.btnMostrar.TabIndex = 18;
            this.btnMostrar.Text = "Mostrar";
            this.btnMostrar.UseVisualStyleBackColor = true;
            this.btnMostrar.Click += new System.EventHandler(this.btnMostrar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(818, 453);
            this.Controls.Add(this.btnMostrar);
            this.Controls.Add(this.btnD);
            this.Controls.Add(this.btnC);
            this.Controls.Add(this.btnB);
            this.Controls.Add(this.btnA);
            this.Controls.Add(this.btnSet2);
            this.Controls.Add(this.btnSet);
            this.Controls.Add(this.BtnReiniciar);
            this.Controls.Add(this.btnBorrar);
            this.Controls.Add(this.lblAciertos);
            this.Controls.Add(this.lblCoincidencias);
            this.Controls.Add(this.lblResultado);
            this.Controls.Add(this.txtResultado);
            this.Controls.Add(this.lblPalabraDos);
            this.Controls.Add(this.lblPalabraUno);
            this.Controls.Add(this.txtDestino);
            this.Controls.Add(this.txtOrigen);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtOrigen;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.TextBox txtDestino;
        private System.Windows.Forms.Label lblPalabraUno;
        private System.Windows.Forms.Label lblPalabraDos;
        private System.Windows.Forms.TextBox txtResultado;
        private System.Windows.Forms.Label lblResultado;
        private System.Windows.Forms.Label lblCoincidencias;
        private System.Windows.Forms.Label lblAciertos;
        private System.Windows.Forms.Button btnBorrar;
        private System.Windows.Forms.Button BtnReiniciar;
        private System.Windows.Forms.Button btnSet;
        private System.Windows.Forms.Button btnSet2;
        private System.Windows.Forms.Button btnA;
        private System.Windows.Forms.Button btnB;
        private System.Windows.Forms.Button btnC;
        private System.Windows.Forms.Button btnD;
        private System.Windows.Forms.Button btnMostrar;
    }
}

