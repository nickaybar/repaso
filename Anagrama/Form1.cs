﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Anagrama
{

    public partial class Form1 : Form
    {
        string palabraUno;
        string palabraDos;
        string resultado;
        int _idx;

        public Form1()
        {
            InitializeComponent();
        }


        private void btnA_Click(object sender, EventArgs e)
        {
            if (_idx == 1)
            {
                txtOrigen.Focus();
                txtOrigen.Text += ((Button)sender).Text;
                palabraUno = txtOrigen.Text;
            }
            else if (_idx == 2)
            {
                txtDestino.Focus();
                txtDestino.Text += ((Button)sender).Text;
                palabraDos = txtDestino.Text;
            }
        }

        private void btnSet_Click(object sender, EventArgs e)
        {
            _idx = 1;
        }

        private void btnSet2_Click(object sender, EventArgs e)
        {
            _idx = 2;
        }


        private void btnMostrar_Click(object sender, EventArgs e)
        {
            if (palabraUno.Length == palabraDos.Length)
            {
                for (int i = 0; i < palabraUno.Length; i++)
                {
                    if (palabraUno[i] == palabraDos[i])
                    {
                        txtResultado.Text += palabraUno[i];
                        resultado += txtResultado.Text;
                    }
                    else
                    {
                        txtResultado.Text += "_ ";
                        resultado += txtResultado.Text;
                    }
                }
            }
            else
            {
                MessageBox.Show("Las palabras no poseen la misma longitud");
            }
        }
        private void Limpiar()
        {
            txtResultado.Clear();
            txtOrigen.Clear();
            txtDestino.Clear();
        }
        private void BtnReiniciar_Click(object sender, EventArgs e)
        {
            palabraUno = string.Empty;
            palabraDos = string.Empty;
            Limpiar();
            _idx = 0;
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            if (_idx == 1)
            {
                if (txtOrigen.Text.Length > 0)
                {
                    txtOrigen.Text = txtOrigen.Text.Remove(txtOrigen.Text.Length - 1);
                }
                else
                {
                    MessageBox.Show("No hay nada para borrar");
                }

            }
            else if (_idx == 2)
            {
                if (txtDestino.TextLength > 0)
                {
                    txtDestino.Text = txtDestino.Text.Remove(txtDestino.TextLength - 1);
                }
                else
                {
                    MessageBox.Show("No hay nada para borrar");
                }
            }
        }
    }
}
