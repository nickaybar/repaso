﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratorio_PreParcial_3
{
	class Program
	{
		static void Main(string[] args)
		{
			//Declarar variables ingreso

			int _personas = 0;
			int _cantidadpeliculas = 0;
			int _contadorNada = 0;
			int _contador1y5 = 0;
			int _contador6mas = 0;
			decimal _porcentajeNada = 0;
			decimal _porcentaje1y5 = 0;
			decimal _porcentaje6mas = 0;
			
			bool _verificarDato = false;
			bool _finalizarPrograma = false;



			while(_finalizarPrograma == false)
			{
				_verificarDato = false;
				while (_verificarDato == false)
				{
					Console.Write("Ingrese la cantidad de peliculas que vio (Finaliza la carga con -1): ");
					if (int.TryParse(Console.ReadLine(), out _cantidadpeliculas) && _cantidadpeliculas >= 0)
					{
						_personas++;
						if(_cantidadpeliculas == 0)
						{
							_contadorNada++;

						}
						else if(_cantidadpeliculas >= 1 && _cantidadpeliculas <= 5)
						{
							_contador1y5++;
						}
						else
						{
							_contador6mas++;
						}
					}
					else
					{
						if(_cantidadpeliculas == -1)
						{
							_finalizarPrograma = true;
						}
						_verificarDato = true;
					}
				}
			}
			//Operaciones
			_porcentajeNada = (_contadorNada * 100m / _personas);
			_porcentaje1y5 = (_contador1y5 * 100m / _personas);
			_porcentaje6mas = (_contador6mas * 100m / _personas);

			//Mostrar resultados
			Console.WriteLine($"\nLa cantidad de personas encuestadas fueron {_personas}");
			Console.WriteLine($"\nDe las {_personas} personas encuestadas, {_contadorNada} no vieron ninguna pelicula");
			Console.WriteLine($"De las {_personas} personas encuestadas, {_contador1y5} vieron entre 1 y 5 peliculas");
			Console.WriteLine($"De las {_personas} personas encuestadas, {_contador6mas} vieron 6 o mas peliculas");

			Console.WriteLine("\nPORCENTAJES");
			Console.WriteLine($"Porcentajes que no vieron peliculas: {_porcentajeNada.ToString("0.00")}%");
			Console.WriteLine($"Porcentajes que vieron entre 1 y 5 peliculas: {_porcentaje1y5.ToString("0.00")}%");
			Console.WriteLine($"Porcentajes que vieron entre 6 o mas peliculas: {_porcentaje6mas.ToString("0.00")}%");
			Console.ReadKey();
		}
	}
}
