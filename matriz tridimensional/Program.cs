﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace matriz_tridimensional
{
	class Program
	{


		public static void CargaMatriz3D(int[,,] mat)
		{
			Random generador = new Random();
			for (int p = 0; p < mat.GetLength(0); p++)
			{
				for (int f = 0; f < mat.GetLength(1); f++)
				{
					for (int c = 0; c < mat.GetLength(2); c++)
					{
						mat[p, f, c] = generador.Next(100);
					}
				}
			}
		}

		public static void MostrarMatriz3D(int[,,] mat)
		{
			Random generador = new Random();
			for (int p = 0; p < mat.GetLength(0); p++)
			{
				Console.WriteLine($"Plano numero {p + 1}");
				Console.WriteLine();
				for (int f = 0; f < mat.GetLength(1); f++)
				{
					for (int c = 0; c < mat.GetLength(2); c++)
					{
						Console.Write(mat[p, f, c].ToString("00") + " ");
					}
					Console.WriteLine();
				}
				Console.WriteLine();
				Console.WriteLine();
			}
		}

	

			static void Main(string[] args)
		{

			const int FILAS = 5;
			const int COLUMNAS = 5;
			const int PLANOS = 3;

			int[,,] vec = new int[PLANOS, FILAS, COLUMNAS];

			int[,] mat = new int[FILAS, COLUMNAS];
			double _promedio = 0;

			Console.Clear();


			CargaMatriz3D(vec);

			MostrarMatriz3D(vec);


			Console.WriteLine();
			Console.WriteLine("Fin del programa...");
			Console.ReadKey();
		}
	}
}