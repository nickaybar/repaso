﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicios_arrays___vectores
{
    class Program
    {
        static void Main(string[] args)
        {
            //vector generado
            const int ELEMENTOS = 50; // valor de la constante elementos
            int[] _vec = new int[ELEMENTOS]; // se le asigna el vector a elementos

            Random _generador = new Random(); //generador aleatorio
            int _promocionados = 0; // almacena la cantidad de alumnos promocionados
            int _aplazados = 0; // alumnos aplazados
            double _porcentajeAplazados = 0.0;
            int _regulares = 0;

            Console.Clear();
            // Vector cargado
            for (int _i =0; _i < ELEMENTOS ; _i++) // tambien se puede poner el nombre de la constante ELEMENTOS en vez de _vec.Length
            {
                _vec[_i] = _generador.Next(11); // genera numeros al azar del 0-10 en el _vec donde se toma la variable _i
            }

            // Mostrar vector
            for (int _i = 0; _i < ELEMENTOS; _i++) // tambien se puede poner el nombre de la constante ELEMENTOS en vez de _vec.Length
            {
                if (_vec[_i] > 6)
                {
                    _promocionados++;
                    
                }
                if (_vec[_i] == 6)
                {
                    _regulares++;

                }
                if (_vec[_i] < 6)
                {
                    _aplazados++;
                    
                }
            }

            if (_aplazados > 0)
            {
                _porcentajeAplazados = (double) (_aplazados * 100) / ELEMENTOS;
            }


            // Mostrar resultados
            Console.WriteLine($"El porcentaje de alumnos aplazados es de {_porcentajeAplazados}%");
            Console.WriteLine($"La cantidad de alumnos promocionados son {_promocionados}");
            Console.WriteLine($"La cantidad de alumnos regulares son {_regulares}");
            Console.WriteLine($"La cantidad de alumnos aplazados son {_aplazados}");
            Console.ReadKey();


        }
    }
}
