﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_Campo
{
    class Program
    {
        static void Main(string[] args)
        { 
            //Declarar
            decimal _ancho = 0m;
            decimal _largo = 0m;
            decimal _areaTrigo = 0m;
            decimal _rendimiento = 0m;
            decimal _rendimientoTrigo = 0m;
            decimal _hectarea = 10000m;
            decimal _superficieSembrada = 0m;

            //Precio
            decimal _precioTrigo = 0m;

            Console.Write("Ingrese el ancho del campo: ");
            if (decimal.TryParse(Console.ReadLine(), out _ancho))
            {
                Console.Write("Ingrese el largo del campo: ");
                if (decimal.TryParse(Console.ReadLine(), out _largo))
                {
                    Console.Write("Ingrese el precio del trigo: ");
                    if (decimal.TryParse(Console.ReadLine(), out _precioTrigo))
                    {
                        Console.Write("Ingrese el rendimiento del trigo en kilos/hectarea: ");
                        if(decimal.TryParse(Console.ReadLine(), out _rendimiento))
                        {
                            _areaTrigo = (_ancho * _largo) / _hectarea;
                            _rendimientoTrigo = (_rendimiento / _areaTrigo);
                            _superficieSembrada = _areaTrigo - _rendimientoTrigo;
                            Console.WriteLine($"El area del trigo ingresado equivale a {_areaTrigo} hectarea(s)");
                            Console.WriteLine($"El Trigo sembrado ocupa un total de {_superficieSembrada} ");
                        }
                    }
                }
            }
            Console.ReadKey();
        }
    }
}
