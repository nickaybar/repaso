﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_5___Pagina_14
{
    class Program
    {
        static void Main(string[] args)
        {
            // Declarar
            decimal _numero = 0;


            string _deseaCalcular = "S";

            //operar

            while(_deseaCalcular.ToUpper() == "S")
            {
                Console.Write("Ingrese un numero para determinar si es par o impar: ");
                if(decimal.TryParse(Console.ReadLine(), out _numero))
                {


                    if(_numero % 2 == 1)
                    {
                        Console.WriteLine("El numero ingresado es impar.");
                    }
                    else if (_numero == 0)
                    {
                        Console.WriteLine("El numero ingresado es cero.");
                    }
                    else
                    {
                        Console.WriteLine("El numero ingresado es par.");
                    }
                }
                Console.WriteLine("\nDesea ingresar otro numero? [S para SI - N para NO]");
                _deseaCalcular = Console.ReadLine();
                
            }
            Console.WriteLine("Presione cualquier tecla para finalizar...");
            Console.ReadKey();
        }
    }
}
