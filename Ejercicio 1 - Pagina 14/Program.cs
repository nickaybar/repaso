﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_1___Pagina_14
{
    class Program
    {
        static void Main(string[] args)
        {
            // Declarar
            decimal _numero = 0;
            decimal _suma = 0;
            decimal _promedio = 0;
            int _contador = 0;
            int _i = 0;


            Console.WriteLine("Ingrese la cantidad de numeros que desea introducir:");
            if(int.TryParse(Console.ReadLine(), out _contador))
            {
                for (_i = 0; _i < _contador; _i++)
                {

                    Console.WriteLine($"Ingrese el numero {_i+1}");
                    if(decimal.TryParse(Console.ReadLine(), out _numero))
                    {
                        _suma += _numero;
                    }
                }
            }


            // Mostrar resultados
            _promedio = _suma / (decimal)_contador; // casteo de conversion _contador a decimal
            Console.WriteLine($"El promedio de las notas ingresadas es: {_promedio} ");

            Console.ReadKey();
        }
    }
}
