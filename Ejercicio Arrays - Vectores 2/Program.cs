﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_Arrays___Vectores_2
{
    class Program
    {
        static public void Saludar(string _cadena, int _pausa)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            for (int _i= 0; _i < _cadena.Length; _i++)
            {
                Console.Write(_cadena[_i]);
                System.Threading.Thread.Sleep(_pausa);
            }
            Console.ResetColor();
        }
        static void Main(string[] args)
        {
            //vector generado
            const int ELEMENTOS = 15; // valor de la constante elementos
            int[] _vec = new int[ELEMENTOS]; // se le asigna el vector a elementos

            Random _generador = new Random(); //generador aleatorio
            int _maximo = -10000;
            int _posMaximo = -1;

            int _minimo = 10000;
            int _posMinimo = -1; // se inicia el vector en -1 y no en 0, porque 0 es una posicion valida

            int _suma = 0;

            Console.Clear();


            for (int _i = 0; _i < ELEMENTOS; _i++) // tambien se puede poner el nombre de la constante ELEMENTOS en vez de _vec.Length
            {
                _vec[_i] = _generador.Next(11); // genera numeros al azar del 0-10 en el _vec donde se toma la variable _i
            }



            // sumatoria del vector
            _suma = 0;
            for (int _i = 0; _i < ELEMENTOS; _i++) // tambien se puede poner el nombre de la constante ELEMENTOS en vez de _vec.Length
            {
                _suma = _suma + _vec[_i];
            }

            // maximo y posicion del maximo
            for (int _i = 0; _i < ELEMENTOS; _i++) // tambien se puede poner el nombre de la constante ELEMENTOS en vez de _vec.Length
            {
                if (_vec[_i] >= _maximo)
                {
                    _maximo = _vec[_i]; // maximo es superado por el vector _i
                    _posMaximo = _i; // nueva posicion la da _i que supero al maximo
                }
                if (_vec[_i] <= _minimo)
                {
                    _minimo = _vec[_i]; // minimo es superado por el vector _i
                    _posMinimo = _i; // nueva posicion la da _i que supero al minimo
                }
            }
            Console.Clear();
            Console.WriteLine();
            Saludar("UNIVERSIDAD TECNOLOGICA NACIONAL", 10);
            Console.WriteLine();
            Saludar("FACULTAD REGIONAL TUCUMAN", 10);
            Console.WriteLine();
            Saludar("AÑO 2018", 10);
            Console.WriteLine();

            Console.WriteLine($"El Maximo numero es {_maximo} y su posicion es {_posMaximo}");
            Console.WriteLine($"El minimo numero es {_minimo} y su posicion es {_posMinimo}");
            Console.ReadKey();
        }
        
    }
}
